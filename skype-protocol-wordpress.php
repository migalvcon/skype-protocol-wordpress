<?php
/*
Plugin Name:  Skype protocol enabler for WordPress
Plugin URI:   https://gitlab.com/migalvcon/skype-protocol-wordpress
Description:  This plugin allows to enable the Skype protocol. See https://msdn.microsoft.com/en-us/library/office/dn745882.aspx
Version:      20180316
Author:       Miguel Angel Alvarez
Author URI:   http://maalvarez.info
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No script kiddies please!');

function enable_skype_protocol($protocols) {
  $protocols[] = 'skype';

  return $protocols;
}
add_filter('kses_allowed_protocols' , 'enable_skype_protocol');
